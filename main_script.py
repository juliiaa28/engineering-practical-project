# -*- coding: utf-8 -*-
"""
Main script

In this script you should create the main structure of your speaker detector

Created on Mon Oct 24 20:40:40 2022

@author: ValBaron10
"""

from joblib import load
import numpy as np
import scipy.signal as signal
import scipy.io as sio
import sys
import matplotlib.pyplot as plt
from features_functions import compute_features

# Get the array information and read the data you want to process


class Localization_Focusing():

	def __init__(self, filename, path_to_file=None):

		self.azimuth, self.elevation, self.distance = np.array(filename[:-6].split("_")[:-1], dtype=float)
		self.azimuth = np.radians(self.azimuth)
		self.elevation = np.radians(self.elevation)
		self.mics_pos = np.loadtxt("micPos.txt")
		self.Nmics = self.mics_pos.shape[0]
		self.filename = filename

		if path_to_file is not None:
			filename = path_to_file + filename
		fs_data, data_int = sio.wavfile.read(filename)
		data_full = data_int / 32767 # To go in Pascal
		self.t_data = np.arange(0, 1/fs_data*data_full.shape[0], 1/fs_data)
		self.fs_data, self.data_full = fs_data, data_full

	def compute_grid(self, grid_length, grid_size):

		self.grid_length = grid_length
		self.grid_size = grid_size
		# Create the grid on which the source will be searched
		self.x = - 0.25 * self.distance * np.cos(-self.elevation) * np.cos(self.azimuth - np.pi/4)
		y = np.arange(-grid_length, grid_length, (2*grid_length)/grid_size)
		z = np.arange(-grid_length, grid_length, (2*grid_length)/grid_size)
		self.grid  = np.vstack(np.meshgrid(self.x,y,z)).reshape(3,-1).T
		return self.grid

	def transfer_function(self, Nfreqs, cut=None):

		if cut is not None:
			self.data_full = self.data_full[cut:]
			self.t_data = self.t_data[cut:]
		
		self.CSM = np.zeros((self.Nmics, self.Nmics, Nfreqs), dtype=complex)
		for i in range(self.Nmics):
		    for j in range(i+1):
		        f, self.CSM[j,i,:] = signal.csd(self.data_full[:, i], self.data_full[:, j], self.fs_data, window='hann', nperseg=2048, scaling='spectrum')
		         # Hermitianisation 
		        self.CSM[i,j,:]=np.conj(self.CSM[j,i,:])

	def localize(self, pref, c0, freq):
		k_freq = 2*np.pi*freq/c0 # Choose # Wave number
		n_freq = int(freq/df)

		# Compute the acoustic map
		MUSIC_map = np.zeros((self.grid.shape[0]))
		# Boucle sur les points de la grille
		for k_pt, point in enumerate(self.grid):
		    R_point =np.sqrt(((point - self.mics_pos)**2).sum(axis=1)) # Choose
		    #g = 1 / R_point * np.exp(1.0j * k_freq * R_point / c0)  # Choose # Création du vecteur de pointage
		    g = 1/R_point*np.exp(1.0j*k_freq*R_point) # C0 is already in K, don't divide again!!!!
		    g_norm = g/ np.linalg.norm(g) # Choose

		    eig_values, eig_vect = np.linalg.eigh(self.CSM[:,:,n_freq]) #increasing way that's why we used 1 instead of -1 below

		    MUSIC_map[k_pt] = 1 / (g_norm[np.newaxis].conjugate() @ eig_vect[:,:-1] @ eig_vect[:,:-1].conjugate().T @ g_norm[np.newaxis].T).real
		    
		MUSIC_map_plt = MUSIC_map.reshape(self.grid_size, self.grid_size)
		return MUSIC_map_plt[::-1,:].T

	def focuse(self, music_map):
		# Compute the features on this signal
		max_pos = np.unravel_index(np.argmax(music_map, axis=None), music_map.shape)
		delta = 2 * self.grid_length / self.grid_size
		map_corner = np.array([self.x, -self.grid_length, -self.grid_length])
		source_pos_3D = map_corner + np.array([0, max_pos[1]*delta, max_pos[0]*delta])

		data_shape = np.shape(self.data_full)
		#Time, Frequency vectors
		t_mics = np.arange(0, data_shape[0]/self.fs_data, 1/self.fs_data)
		f_mics = np.fft.fftfreq(t_mics.shape[-1]) * self.fs_data / 2

		mics_fft = []
		for i in np.arange(self.Nmics):
		    mics_fft.append(np.fft.fft(self.data_full[:,i]))

		focused_mics = np.zeros((data_shape[0], self.Nmics))
		for k_mic, mic_pos in enumerate(self.mics_pos):
		    R = np.sqrt(((mic_pos - source_pos_3D)**2).sum()) # Relative distance from the source
		    Rm = R/c0 # Attenuation of the sphere
		    mic_inv_delay = mics_fft[k_mic] * np.exp(1j * 2 * np.pi * freq * Rm) / R
		    focused_mics[:, k_mic] = np.real( np.fft.ifft(mic_inv_delay))
		return focused_mics

	def extract_features(self, input_sig):

		# Compute the signal in three domains
		sig_sq = input_sig ** 2
		sig_t = input_sig / np.sqrt(sig_sq.sum())  # focused_sig
		sig_f = np.absolute(np.fft.fft(sig_t))
		sig_c = np.absolute(np.fft.fft(sig_f))

		#Features extraxction
		N_feat, features_list = compute_features(sig_t, sig_f[:sig_t.shape[0]//2], sig_c[:sig_t.shape[0]//2])
		return features_list

	def plot_test(self, focused_mics, music_map):

		#Audio Test on single channel and sum of channels
		chann = 0
		original_single = self.data_full[:, chann]
		original_summed = np.sum(self.data_full, axis=1)
		focused_source = np.sum(focused_mics, axis=1)
		t_mics = np.arange(0, self.data_full.shape[0]/self.fs_data, 1/self.fs_data)

		#ticks_lab = ["One Channel", "Sum of Channels", "Sum of Focused Channels"]
		ticks_lab = []
		ticks_pos = []
		power_sigs = []
		compare_sigs = [original_single, original_summed, focused_source]


		#Plot Scenario
		# Line Vector in the direction (true) of the Source in -X
		xline = [0, -self.distance * np.cos(-self.elevation) * np.cos(self.azimuth - np.pi/4)]
		yline = [0, self.distance * np.cos(-self.elevation) * np.sin(self.azimuth - np.pi/4)]
		zline = [0, self.distance * np.sin(-self.elevation)]

		# Identify the position of the maximum on the grid
		pref = 2 * 10 ** (-5)
		max_pos = np.unravel_index(np.argmax(music_map, axis=None), music_map.shape)

		delta = 2 * self.grid_length / self.grid_size
		map_corner = np.array([self.x, -self.grid_length, -self.grid_length])
		disp_from_corner = map_corner + np.array([0, max_pos[1] * delta, max_pos[0] * delta])
		source_pos_3D = np.array(disp_from_corner)

		#Plot the MUSIC Map
		plt.figure()
		plt.title("MUSIC map")
		plt.imshow(10 * np.log10(music_map / pref ** 2), origin="lower", extent=[-2, 2, -2, 2])
		plt.xlabel("y (m)")
		plt.ylabel("z (m)")
		plt.colorbar()

		# Plot the configuration
		plt.figure()
		plt.title("Simulated configuration")
		ax = plt.axes(projection="3d")
		ax.scatter(self.mics_pos[:, 0], self.mics_pos[:, 1], self.mics_pos[:, 2], c='C1')
		ax.scatter(self.grid[:, 0], self.grid[:, 1], self.grid[:, 2], s=1 ** 2, c='C2')
		ax.scatter(source_pos_3D[0], source_pos_3D[1], source_pos_3D[2], s=10 ** 2, c='C0')
		ax.scatter(map_corner[0], map_corner[1], map_corner[2], c='C3')
		ax.plot3D(xline, yline, zline, 'gray')
		ax.set_xlabel('x')
		ax.set_ylabel('y')
		plt.legend(["Array", "Grid", "Source", "Grid Corner", "Vector To Source"])

		# Plot the array received signals
		plt.figure()
		plt.title("Signal Focusing Results on file {}".format(self.filename))
		for k_sig, sig in enumerate(compare_sigs):
		    plt.plot(t_mics, k_sig + sig)
		    ticks_pos.append( k_sig)
		    ticks_lab.append("Signal. {}".format(k_sig + 1))
		    power_sigs.append(np.sum(sig ** 2) / len(sig))
		plt.yticks(ticks_pos, ticks_lab)
		plt.xlabel("Time [s]")
		plt.legend(["One Channel", "Sum of Channels", "Sum of Focused Channels"], loc='lower left')
		#plt.show()

		print("\n-------------------------------------------------------")
		print("               FOCUSING PROCESS RESULTS      ")
		print("-------------------------------------------------------\n")
		print("The power of a single channel is: ", power_sigs[0])
		print("The power of summed channels is: ", power_sigs[1])
		print("The power of summed focused channels is: ", power_sigs[2])


if __name__ == "__main__" :

	if len(sys.argv) > 1:
		filename = str(sys.argv[1])
	else:
		# example
		filename = "45_0_1.2__1.wav"

	fs=44000
	bs = 2048 # Block size for the Welch periodogram
	df = fs/bs # Frequency resolution
	Nfreqs = int(fs/df//2+1)
	localization = Localization_Focusing(filename, "DREGON_clean_recordings_speech/")

	# Create the computation grid you want to work on

	localization.compute_grid(10, 50)


	# Compute the transfer function between the source and the mics

	localization.transfer_function(Nfreqs)

	# Choose the frequency to process

	pref = 2*10**(-5)
	c0=343
	freq = 1000

	# Localize the source

	music_map = localization.localize(pref, c0, freq)

	# Obtain a focused signal in the direction of arrival

	focused_mics = localization.focuse(music_map)

	# Load the scaler and SVM model to test the class of your source

	scaler = load("SCALER")
	model = load("SVM_MODEL")

	# Get the class of your source

	signal_features = localization.extract_features(focused_mics[:,0])
	signal_features = np.array(signal_features)[np.newaxis, :]
	signal_prediction = model.predict(signal_features)

	localization.plot_test(focused_mics, music_map)

	# Analyze the results

	if signal_prediction == 0:
		pred = "White Noise"
	else:
		pred = "Speech"


	print("\n-------------------------------------------------------")
	print("               CLASSIFICATION RESULTS      ")
	print("-------------------------------------------------------\n")
	print("The signal was classified as: ", pred)
	print("\n-------------------------------------------------------\n")

	plt.show()
