# -*- coding: utf-8 -*-
"""
Script to learn a model with Scikit-learn.

Created on Mon Oct 24 20:51:47 2022

@author: ValBaron10
"""

from operator import concat
from sklearn import preprocessing
from sklearn import svm
from joblib import dump
from features_functions import compute_features
from glob import glob
import scipy.io.wavfile
import numpy as np

# Get all the file names in the directory text_format 
noise_files = glob('DREGON_clean_recordings_whitenoise/*')
speech_files = glob('DREGON_clean_recordings_speech/*')

# remove the last element for testing
train_noise_files  = noise_files[:-1]
train_speech_files = speech_files[:-1]


learningFeatures = []

def extract_features(input_sig):
    # Compute the signal in three domains
    sig_sq = input_sig**2
    sig_t = input_sig / np.sqrt(sig_sq.sum())  #focused_sig
    sig_f = np.absolute(np.fft.fft(sig_t))
    sig_c = np.absolute(np.fft.fft(sig_f))
    
    # Compute the features and store them
    features_list = []
    N_feat, features_list = compute_features(sig_t, sig_f[:sig_t.shape[0]//2], sig_c[:sig_t.shape[0]//2])
    return np.array(features_list)[np.newaxis,:]

# LOOP OVER THE SIGNALS
#for all signals:
for s in train_noise_files+train_speech_files:
    # Get an input signal
    fs_data, data_int = scipy.io.wavfile.read(s)
    input_sig = data_int[:,0] # Import a signal
    learningFeatures.append(extract_features(input_sig))

# Store the obtained features in a np.arrays
learningFeatures = np.squeeze(np.array(learningFeatures))# 2D np.array with feature values in it

# Store the labels
learningLabels = np.concatenate((np.zeros((len(train_noise_files))), np.ones((len(train_speech_files)))))# np.array with labels in it

# Encode the class names
labelEncoder = preprocessing.LabelEncoder().fit(learningLabels) # To add the "Gros Drone" class during learning stage to avoid a crash in test phase
learningLabelsStd = labelEncoder.transform(learningLabels)

# Learn the model
model = svm.SVC(C=10, class_weight=None, probability=False)
scaler = preprocessing.StandardScaler(with_mean=True).fit(learningFeatures)
learningFeatures_scaled = scaler.transform(learningFeatures)
model.fit(learningFeatures_scaled, learningLabelsStd)

fs_data1, data_int1 = scipy.io.wavfile.read(noise_files[-1])
fs_data2, data_int2 = scipy.io.wavfile.read(speech_files[-1])
if model.predict(extract_features(data_int1[:, 0])):
    file1 = "Speech"
else:
    file1 = "Noise"
if model.predict(extract_features(data_int2[:, 0])):
    file2 = "Speech"
else:
    file2 = "Noise"
print("Test noise file predicted as: ", file1)
print("Test speech file predicted as: ", file2)

# Export the scaler and model
dump(scaler, "SCALER")
dump(model, "SVM_MODEL")

